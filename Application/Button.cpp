#include "stdafx.h"
#include "Button.h"


Button::Button()
{
}

void Button::Init(CWnd& _parent, CRect& _rect)
{
	static int id = 10;
	++id;
	this->Create(_T("btn"), WS_CHILD | WS_VISIBLE | WS_TABSTOP |
		ES_AUTOHSCROLL | WS_BORDER | BS_PUSHBUTTON, _rect, &_parent, id);
}

Button::~Button(void)
{
}

BEGIN_MESSAGE_MAP(Button, CButton)
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_LBUTTONDBLCLK()
END_MESSAGE_MAP()


void Button::OnLButtonDown(UINT nFlags, CPoint point)
{
	buttonEventHandler.buttonPressed();

	CButton::OnLButtonDown(nFlags, point);
}

void Button::OnLButtonDblClk(UINT nFlags, CPoint point)
{
	buttonEventHandler.buttonPressed();

	CButton::OnLButtonDown(nFlags, point);
}

void Button::OnLButtonUp(UINT nFlags, CPoint point)
{
	buttonEventHandler.buttonReleased();

	CButton::OnLButtonUp(nFlags, point);
}
