#pragma once
#include "afxwin.h"
#include <functional>
#include <vector>
#include <memory>
#include "D2D1Drawable.h"

/*-------------------------------------------------*/

class CollectionViewItem;

struct CollectionEventsHandler
{
	std::function<void (CollectionViewItem& _item)> onItemClicked;
};

class AnimationController;
class CollectionView : public D2D1Drawable<CWnd>
{
public:
	CollectionView(void);
	void Init(CWnd & _parent, CRect& _rect);
	~CollectionView(void);
	
public:
	void addCollectionItem( CollectionViewItem* _item );
	
	void StartScrolling(bool _forward);
	void StopScrolling();

	int GetContentOffset() const;
	void SetContentOffset(int _offset);
	int GetContentSize() const;

	CRect GetBounds() const;

private:
	DECLARE_MESSAGE_MAP()
	afx_msg void OnTimer(UINT);
	afx_msg void OnPaint();
	afx_msg BOOL PreCreateWindow(CREATESTRUCT& cs); 

public:
	CollectionEventsHandler collectionEventsHandler;

private:
	std::vector< std::unique_ptr<CollectionViewItem> > m_collectionItems;

	int m_contentOffset;
	int m_contentSize;
	int m_margin;
	int m_optimalElementsCount;
	int m_optimalElementWidth;
	int m_optimalElementHeight;

	std::unique_ptr<AnimationController> m_animationController;

	CComPtr<IDCompositionVisual> m_visual;
};

/*-------------------------------------------------*/
