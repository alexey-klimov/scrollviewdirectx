#include "stdafx.h"
#include "CollectionItemView.h"
#include "CollectionView.h"

CollectionViewItem::CollectionViewItem( std::wstring&& _imageFileName )
	: m_imageFileName(_imageFileName)
	, m_collectionView(nullptr)
	, m_bitmapScaleX(0)
	, m_bitmapScaleY(0)
{
}

CollectionViewItem::CollectionViewItem(CollectionViewItem& _other)
	: m_imageFileName(_other.m_imageFileName)
	, m_bitmapScaleX(_other.m_bitmapScaleX)
	, m_bitmapScaleY(_other.m_bitmapScaleY)
{
}

HRESULT CollectionViewItem::Init(CollectionView & _collectionView, CRect& _rect, CComPtr<IDCompositionDevice> _device)
{
	m_collectionView = &_collectionView;

	static int id = 100;
	id++;
	
	this->Create(NULL, _T("collectionItem"), WS_CHILD | WS_VISIBLE, 
		_rect, m_collectionView, id);

	int bitmapWidth = 0;
	int bitmapHeight = 0;
	
	HRESULT hr = S_OK;
	if (SUCCEEDED(hr))
	{
		hr = m_collectionView->CreateSurfaceFromFile(m_imageFileName.data(), &bitmapWidth, &bitmapHeight, &m_surface);
	}

	if (SUCCEEDED(hr))
	{
		hr = _device->CreateVisual(&m_visual);
	}

	if (SUCCEEDED(hr))
	{
		hr = m_visual->SetContent(m_surface);
	}

	if (SUCCEEDED(hr))
	{
		hr = this->scaleAndTranslate(bitmapWidth, bitmapHeight, _rect, _device);
	}

	return hr;
}

HRESULT CollectionViewItem::scaleAndTranslate(int _bitmapWidth, int _bitmapHeight, CRect& _rect, CComPtr<IDCompositionDevice> _device)
{
	HRESULT hr = S_OK;
	
	CComPtr<IDCompositionTranslateTransform> translateTransform;

	if (SUCCEEDED(hr))
	{
		hr = _device->CreateTranslateTransform(&translateTransform);
	}

	if (SUCCEEDED(hr))
	{
		hr = translateTransform->SetOffsetX((float)_rect.left);
	}

	if (SUCCEEDED(hr))
	{
		hr = translateTransform->SetOffsetY((float)_rect.top);
	}
	
	CComPtr<IDCompositionScaleTransform> scaleTransform;
	
	if (SUCCEEDED(hr))
	{
		hr = _device->CreateScaleTransform(&scaleTransform);
	}

	if (!m_bitmapScaleX)
		m_bitmapScaleX = (float)_rect.Width() / _bitmapWidth;

	if (SUCCEEDED(hr))
	{
		hr = scaleTransform->SetScaleX(m_bitmapScaleX);
	}

	if (!m_bitmapScaleY)
		m_bitmapScaleY = (float)_rect.Height() / _bitmapHeight;

	if (SUCCEEDED(hr))
	{
		hr = scaleTransform->SetScaleY(m_bitmapScaleY);
	}
	
	IDCompositionTransform *transforms[] =
	{
		scaleTransform,
		translateTransform,
	};

	CComPtr<IDCompositionTransform> transformGroup;
	if (SUCCEEDED(hr))
	{
		_device->CreateTransformGroup(transforms, sizeof(transforms) / sizeof(transforms[0]), &transformGroup);
	}

	if (SUCCEEDED(hr))
	{
		hr = m_visual->SetTransform(transformGroup);
	}

	return hr;
}

CollectionViewItem::~CollectionViewItem()
{
}

/*-------------------------------------------------*/

BEGIN_MESSAGE_MAP(CollectionViewItem, CWnd)
	ON_WM_PAINT()
	ON_WM_LBUTTONUP()
END_MESSAGE_MAP()

/*-------------------------------------------------*/

BOOL CollectionViewItem::PreCreateWindow(CREATESTRUCT& cs) 
{
	if (!CWnd::PreCreateWindow(cs))
		return FALSE;

	cs.lpszClass = AfxRegisterWndClass(0);

	return TRUE;
}

/*-------------------------------------------------*/

void CollectionViewItem::OnPaint()
{
	CPaintDC dc(this); // device context for painting
}

void CollectionViewItem::OnLButtonUp(UINT nFlags, CPoint point)
{
	m_collectionView->collectionEventsHandler.
		onItemClicked(*this);

	CWnd::OnLButtonUp(nFlags, point);
}

/*-------------------------------------------------*/