
// MainView.cpp : implementation of the MainView class
//

#include "stdafx.h"
#include "Application.h"
#include "MainView.h"
#include "CollectionItemView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

/*-------------------------------------------------*/

// MainView

MainView::MainView()
	: m_width(800)
	, m_margin(10)
	, m_buttonSize(static_cast<int>(m_width*0.1), static_cast<int>(m_width*0.1))
	, m_collectionSize(m_width - 4*m_margin - 2*m_buttonSize.cx, (m_width - 4 * m_margin - 2 * m_buttonSize.cx) / 4)
{
}

MainView::~MainView()
{
}

void MainView::Init()
{	
	InitLeftButton();
	InitRightButton();
	InitCollections();
}

void MainView::InitLeftButton()
{
	m_leftButton.Init( *this, CRect(CPoint(m_margin, m_margin), m_buttonSize) );
	m_leftButton.buttonEventHandler.buttonPressed = [this](){
		m_scrollView.StartScrolling(false);
	};
	m_leftButton.buttonEventHandler.buttonReleased = [this](){
		m_scrollView.StopScrolling();
	};
}

void MainView::InitRightButton()
{
	m_rightButton.Init( *this, CRect(
		CPoint(m_width - m_margin - m_buttonSize.cx, m_margin), 
		m_buttonSize) 
	);
	m_rightButton.buttonEventHandler.buttonPressed = [this](){ 
		m_scrollView.StartScrolling(true);
	};
	m_rightButton.buttonEventHandler.buttonReleased = [this](){
		m_scrollView.StopScrolling();
	};
}

void MainView::InitCollections()
{
	m_scrollView.Init( *this, CRect(
		CPoint(m_buttonSize.cx + 2*m_margin, m_margin), 
		m_collectionSize) 
	);
	m_catalogView.Init( *this, CRect(
		CPoint(m_buttonSize.cx + 2*m_margin, m_collectionSize.cy + 2*m_margin), 
		m_collectionSize) 
	);
	m_catalogView.collectionEventsHandler.onItemClicked = [this](CollectionViewItem& _item){
		this->m_scrollView.addCollectionItem( new CollectionViewItem(_item) );
	};

	this->m_catalogView.addCollectionItem( new CollectionViewItem(_T("one.jpg")) );
	this->m_catalogView.addCollectionItem(new CollectionViewItem(_T("two.jpg")));
	/*this->m_catalogView.addCollectionItem( new CollectionViewItem );
	this->m_catalogView.addCollectionItem( new CollectionViewItem );*/
}


/*-------------------------------------------------*/

BEGIN_MESSAGE_MAP(MainView, CWnd)
	ON_WM_PAINT()
END_MESSAGE_MAP()

/*-------------------------------------------------*/
// MainView message handlers

BOOL MainView::PreCreateWindow(CREATESTRUCT& cs) 
{
	if (!CWnd::PreCreateWindow(cs))
		return FALSE;

	cs.dwExStyle |= WS_EX_CLIENTEDGE;
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW|CS_VREDRAW|CS_DBLCLKS, 
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW+1), NULL);

	return TRUE;
}

void MainView::OnPaint() 
{
	CPaintDC dc(this); // device context for painting
}


/*-------------------------------------------------*/