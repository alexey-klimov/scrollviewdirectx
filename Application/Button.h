#pragma once
#include "afxwin.h"
#include <functional>

struct ButtonEventHandler
{
	std::function<void(void)> buttonPressed;
	std::function<void(void)> buttonReleased;
};

/*-------------------------------------------------*/

class Button :
	public CButton
{
public:
	Button();
	void Init(CWnd& _parent, CRect& _rect);
	~Button(void);
	DECLARE_MESSAGE_MAP()
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	
public:
	ButtonEventHandler buttonEventHandler;
};

/*-------------------------------------------------*/