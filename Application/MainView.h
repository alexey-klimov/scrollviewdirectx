
// MainView.h : interface of the MainView class
//

#pragma once

#include "CollectionView.h"
#include "Button.h"

// MainView window
class MainView : public CWnd
{
// Construction
public:
	MainView();
	void Init();
// Attributes
public:

// Operations
public:

// Overrides
	protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

// Implementation
public:
	virtual ~MainView();

	// Generated message map functions
protected:
	afx_msg void OnPaint();
	DECLARE_MESSAGE_MAP()

private:
	void InitLeftButton();
	void InitRightButton();
	void InitCollections();

private:
	const int m_width;
	const int m_margin;
	const CSize m_buttonSize;
	const CSize m_collectionSize;

private:
	Button m_leftButton;
	Button m_rightButton;
	CollectionView m_scrollView;
	CollectionView m_catalogView;
};

