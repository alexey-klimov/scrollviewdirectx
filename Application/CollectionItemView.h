#pragma once
#include "afxwin.h"
#include <string>
#include "D2D1Drawable.h"

class CollectionView;
class CollectionViewItem : public CWnd
{
public:
	CollectionViewItem(std::wstring&& _imageFileName);
	CollectionViewItem(CollectionViewItem& _other);
	~CollectionViewItem();
	
private:
	CollectionViewItem(CollectionViewItem&& _other);
	CollectionViewItem& operator=(CollectionViewItem&);
	CollectionViewItem& operator=(CollectionViewItem&&);
	DECLARE_MESSAGE_MAP()
	afx_msg BOOL PreCreateWindow(CREATESTRUCT& cs);
	afx_msg void OnPaint();
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);

private:
	friend CollectionView;
	HRESULT Init(CollectionView & _collectionView, CRect& _rect, CComPtr<IDCompositionDevice> _device);
	HRESULT CollectionViewItem::scaleAndTranslate(int _bitmapWidth, int _bitmapHeight, CRect& _rect, CComPtr<IDCompositionDevice> _device);
	
private:
	std::wstring m_imageFileName;
	CComPtr<IDCompositionVisual> m_visual;
	CComPtr<IDCompositionSurface> m_surface;
	CollectionView* m_collectionView;
	float m_bitmapScaleX;
	float m_bitmapScaleY;
};
