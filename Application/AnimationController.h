#pragma once
#include "afxwin.h"

#include <functional>

/*-------------------------------------------------*/

class AnimationController
{ 
	HWND hwnd;
	bool forward;
	bool running;
	
	int delta;
	int deltaAcceleration;
	int maxDelta;
	
	//Precision mode
	const int fastModeDelta;
	int countsToEnd;
	
	int timerId;

public:
	
	std::function<void(int)> offsetSetter;
	std::function<int(void)> offsetGetter;
	
	AnimationController(HWND _hwnd)
		: hwnd(_hwnd), forward(true), running(false)
		, delta(4), deltaAcceleration(1), maxDelta(40)
		, fastModeDelta(30), countsToEnd(-1)
		, timerId(132), offsetSetter([](int){}), offsetGetter([](){return 0;})
	{
		static int timerUniqueID = 210; 
		++timerUniqueID;
		timerId = timerUniqueID;
	}

	bool isForward()
	{
		return forward;
	}

	void start( bool _forward, int _delta = 4, int _deltaAcceleration = 1, int _maxDelta = 40 )
	{
		if (running)
			return;
		forward = _forward;
		running = true;
		delta = _delta;
		deltaAcceleration = _deltaAcceleration;
		maxDelta = _maxDelta;
		countsToEnd = -1;
		SetTimer(hwnd, timerId, 10, nullptr);
	}

	void startPrecisely( int toValue)
	{
		if (running)
			return;
	
		running = true;
		delta = fastModeDelta;
		deltaAcceleration = 0;
		
		countsToEnd = toValue - offsetGetter();
		forward = countsToEnd > 0;
		if (countsToEnd < 0) 
			countsToEnd = -countsToEnd;
		
		SetTimer(hwnd, timerId, 10, nullptr);
	}
	
	void stop()
	{
		if (!running)
			return;
		running = false;
		countsToEnd = -1;
		KillTimer(hwnd, timerId);
	}

	void onTimer()
	{
		if (delta < maxDelta)
			delta = delta + deltaAcceleration;
		
		//printf("Before %d %d\n", offsetGetter(), countsToEnd);
		if (countsToEnd < 0)
		{
			offsetSetter( offsetGetter() + (forward ? delta : -delta ));
			return;
		}
		else if (countsToEnd == 0)
		{
			stop();
			return;
		} 
		else if (countsToEnd < fastModeDelta)
		{
			offsetSetter( offsetGetter() + (forward ? countsToEnd : -countsToEnd ));
			stop();
			//printf("After %d\n", offsetGetter());
			return;
		}
		else
		{
			offsetSetter( offsetGetter() + (forward ? delta : -delta ));
			countsToEnd -= delta;
			return;
		}
	}
};

/*-------------------------------------------------*/