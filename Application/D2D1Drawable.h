#pragma once

#include <dcomp.h>
#include <d3d11_1.h>
#include <d2d1_1.h>

template<typename TBase>
class D2D1Drawable : public TBase
{
public:
	D2D1Drawable();
	D2D1Drawable(D2D1Drawable& _other);

private:
	D2D1Drawable(D2D1Drawable&& _other);
	D2D1Drawable& operator=(D2D1Drawable&);
	D2D1Drawable& operator=(D2D1Drawable&&);

public:
	HRESULT InitD2D1Drawable();
	
	HRESULT CreateD3D11Device();
    HRESULT CreateD2D1Factory();
    HRESULT CreateD2D1Device();
    HRESULT CreateWICFactory();
	HRESULT CreateDCompositionDevice();
	HRESULT CreateDCompositionRenderTarget();

	void DestroyDCompositionRenderTarget();
    void DestroyDCompositionDevice();
	void DestroyWICFactory();
    void DestroyD2D1Device();
    void DestroyD2D1Factory();
    void DestroyD3D11Device();

	HRESULT CreateSurfaceFromFile(const WCHAR *filename, int *bitmapWidth, int *bitmapHeight, IDCompositionSurface **surface);
    HRESULT CreateD2D1BitmapFromFile(const WCHAR *filename, ID2D1Bitmap **bitmap);

protected:
	// Direct2D
    CComPtr<ID3D11Device> _d3d11Device;
    CComPtr<ID3D11DeviceContext> _d3d11DeviceContext;
    
	CComPtr<ID2D1Factory1> _d2d1Factory;
    CComPtr<ID2D1Device> _d2d1Device;
    CComPtr<ID2D1DeviceContext> _d2d1DeviceContext;

	// Loading bitmaps
    CComPtr<IWICImagingFactory> _wicFactory;

	// Compisition
    CComPtr<IDCompositionDevice> _device;
    CComPtr<IDCompositionTarget> _target;

};

