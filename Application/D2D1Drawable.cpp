#include "stdafx.h"
#include "D2D1Drawable.h"

template<typename TBase>
D2D1Drawable<TBase>::D2D1Drawable()
{
}

template<typename TBase>
D2D1Drawable<TBase>::D2D1Drawable(D2D1Drawable& _other)
	: _d3d11Device(_other._d3d11Device)
    , _d3d11DeviceContext(_other._d3d11DeviceContext)
	, _d2d1Factory(_other._d2d1Factory)
	, _d2d1Device(_other._d2d1Device)
	, _d2d1DeviceContext(_other._d2d1DeviceContext)
	, _wicFactory(_other._wicFactory)
	, _device(_other._device)
{
}

template<typename TBase>
HRESULT D2D1Drawable<TBase>::InitD2D1Drawable()
{
    HRESULT hr = S_OK;

    if (SUCCEEDED(hr))
    {
        hr = CreateD3D11Device();
    }

    if (SUCCEEDED(hr))
    {
        hr = CreateD2D1Factory();
    }

    if (SUCCEEDED(hr))
    {
        hr = CreateD2D1Device();
    }

    if (SUCCEEDED(hr))
    {
        hr = CreateWICFactory();
    }

    if (SUCCEEDED(hr))
    {
        hr = CreateDCompositionDevice();
    }

    if (SUCCEEDED(hr))
    {
        hr = CreateDCompositionRenderTarget();
    }

    if (SUCCEEDED(hr))
    {
        hr = _device->Commit();
    }

    return hr;
}

template<typename TBase>
HRESULT D2D1Drawable<TBase>::CreateD3D11Device()
{
    if (_d3d11Device)
		return S_OK;
	HRESULT hr = S_OK;

    D3D_DRIVER_TYPE driverTypes[] = 
    {
        D3D_DRIVER_TYPE_HARDWARE,
        D3D_DRIVER_TYPE_WARP,
    };

    D3D_FEATURE_LEVEL featureLevelSupported;

    for (int i = 0; i < sizeof(driverTypes) / sizeof(driverTypes[0]); ++i)
    {
        CComPtr<ID3D11Device> d3d11Device;
        CComPtr<ID3D11DeviceContext> d3d11DeviceContext;

        hr = D3D11CreateDevice(
            nullptr,
            driverTypes[i], 
            NULL, 
            D3D11_CREATE_DEVICE_BGRA_SUPPORT,  
            NULL, 
            0, 
            D3D11_SDK_VERSION,
            &d3d11Device,
            &featureLevelSupported,
            &d3d11DeviceContext);

        if (SUCCEEDED(hr))
        {
            _d3d11Device = d3d11Device.Detach();
            _d3d11DeviceContext = d3d11DeviceContext.Detach();

            break;
        }
    }

    return hr;
}

template<typename TBase>
HRESULT D2D1Drawable<TBase>::CreateD2D1Factory()
{
    if (_d2d1Factory)
		return S_OK;
	return D2D1CreateFactory(D2D1_FACTORY_TYPE_SINGLE_THREADED, &_d2d1Factory);
}

template<typename TBase>
HRESULT D2D1Drawable<TBase>::CreateD2D1Device()
{
     if (_d2d1Device)
		return S_OK;
	HRESULT hr = ((_d3d11Device == nullptr) || (_d2d1Factory == nullptr)) ? E_UNEXPECTED : S_OK;

    CComPtr<IDXGIDevice> dxgiDevice;

    if (SUCCEEDED(hr))
    {
        hr = _d3d11Device->QueryInterface(&dxgiDevice);
    }

    if (SUCCEEDED(hr))
    {
        hr = _d2d1Factory->CreateDevice(dxgiDevice, &_d2d1Device);
    }

    if (SUCCEEDED(hr))
    {
        hr = _d2d1Device->CreateDeviceContext(D2D1_DEVICE_CONTEXT_OPTIONS_NONE, &_d2d1DeviceContext);
    }

    return hr;
}

template<typename TBase>
HRESULT D2D1Drawable<TBase>::CreateWICFactory()
{
     if (_wicFactory)
		return S_OK;
	HRESULT hr = CoInitializeEx(nullptr, COINIT_APARTMENTTHREADED | COINIT_DISABLE_OLE1DDE);
	if (SUCCEEDED(hr))
	{
		CoCreateInstance(
			CLSID_WICImagingFactory,
			nullptr,
			CLSCTX_INPROC_SERVER,
			IID_PPV_ARGS(&_wicFactory)
		);
	}
	
	return hr;
}

//-----------------------------------------------------------
// Creates a DirectComposition device
//-----------------------------------------------------------
template<typename TBase>
HRESULT D2D1Drawable<TBase>::CreateDCompositionDevice()
{
    if (_device)
		return S_OK;
	HRESULT hr = (_d3d11Device == nullptr) ? E_UNEXPECTED : S_OK;
    
    CComPtr<IDXGIDevice> dxgiDevice;

    if (SUCCEEDED(hr))
    {
        hr = _d3d11Device->QueryInterface(&dxgiDevice);
    }

    if (SUCCEEDED(hr))
    {
        hr = DCompositionCreateDevice(dxgiDevice, __uuidof(IDCompositionDevice), reinterpret_cast<void **>(&_device));
    }

    return hr;
}


//--------------------------------------------------------
// Creates an render target for DirectComposition which
// is an hwnd in this case
//--------------------------------------------------------
template<typename TBase>
HRESULT D2D1Drawable<TBase>::CreateDCompositionRenderTarget()
{
	HRESULT hr = ((_device == nullptr) || (m_hWnd == NULL)) ? E_UNEXPECTED : S_OK;

    if (SUCCEEDED(hr))
    {
        hr = _device->CreateTargetForHwnd(m_hWnd, TRUE, &_target);
    }

    return hr;
}

template<typename TBase>
void D2D1Drawable<TBase>::DestroyDCompositionRenderTarget()
{
    _target = nullptr;
}

template<typename TBase>
void D2D1Drawable<TBase>::DestroyDCompositionDevice()
{
    _device = nullptr;
}

template<typename TBase>
void D2D1Drawable<TBase>::DestroyWICFactory()
{
    _wicFactory = nullptr;
}

template<typename TBase>
void D2D1Drawable<TBase>::DestroyD2D1Device()
{
    _d2d1DeviceContext = nullptr;
    _d2d1Device = nullptr;
}

template<typename TBase>
void D2D1Drawable<TBase>::DestroyD2D1Factory()
{
    _d2d1Factory = nullptr;
}

template<typename TBase>
void D2D1Drawable<TBase>::DestroyD3D11Device()
{
    _d3d11DeviceContext = nullptr;
    _d3d11Device = nullptr;
}

template<typename TBase>
HRESULT D2D1Drawable<TBase>::CreateSurfaceFromFile(const WCHAR *filename, int *bitmapWidth, int *bitmapHeight, IDCompositionSurface **surface)
{
    HRESULT hr = ((bitmapWidth == nullptr) || (bitmapHeight == nullptr) || (surface == nullptr)) ? E_POINTER : S_OK;

    if (SUCCEEDED(hr))
    {
        *bitmapWidth = 0;
        *bitmapHeight = 0;
        *surface = NULL;

        hr = (filename == nullptr) ? E_INVALIDARG : S_OK;
    }

    CComPtr<ID2D1Bitmap> d2d1Bitmap;
    D2D1_SIZE_F bitmapSize = { 0 };

    if (SUCCEEDED(hr))
    {
        hr = CreateD2D1BitmapFromFile(filename, &d2d1Bitmap);
    }

    CComPtr<IDCompositionSurface> surfaceTile;

    if (SUCCEEDED(hr))
    {
        bitmapSize = d2d1Bitmap->GetSize();

        hr = _device->CreateSurface(
            static_cast<UINT>(bitmapSize.width), 
            static_cast<UINT>(bitmapSize.height), 
            DXGI_FORMAT_R8G8B8A8_UNORM, 
            DXGI_ALPHA_MODE_IGNORE, 
            &surfaceTile);
    }

    CComPtr<IDXGISurface> dxgiSurface;
    POINT offset;

    if (SUCCEEDED(hr))
    {
        RECT rect = { 0, 0, static_cast<LONG>(bitmapSize.width), static_cast<LONG>(bitmapSize.height) };
        
        hr = surfaceTile->BeginDraw(&rect, __uuidof(IDXGISurface), reinterpret_cast<void **>(&dxgiSurface), &offset);
    }

    CComPtr<ID2D1Bitmap1> d2d1Target;

    if (SUCCEEDED(hr))
    {
        FLOAT dpiX = 0.0f;
        FLOAT dpiY = 0.0f;

        _d2d1Factory->GetDesktopDpi(&dpiX, &dpiY);

        D2D1_BITMAP_PROPERTIES1 bitmapProperties = D2D1::BitmapProperties1(
            D2D1_BITMAP_OPTIONS_TARGET | D2D1_BITMAP_OPTIONS_CANNOT_DRAW,
            D2D1::PixelFormat(DXGI_FORMAT_R8G8B8A8_UNORM, D2D1_ALPHA_MODE_IGNORE),
            dpiX,
            dpiY);

        hr = _d2d1DeviceContext->CreateBitmapFromDxgiSurface(dxgiSurface, &bitmapProperties, &d2d1Target);

        if (SUCCEEDED(hr))
        {
            _d2d1DeviceContext->SetTarget(d2d1Target);

            _d2d1DeviceContext->BeginDraw();

            _d2d1DeviceContext->DrawBitmap(
                d2d1Bitmap,
                D2D1::RectF(
                    offset.x + 0.0f, 
                    offset.y + 0.0f, 
                    offset.x + bitmapSize.width, 
                    offset.y + bitmapSize.height));

            hr = _d2d1DeviceContext->EndDraw();
        }

        surfaceTile->EndDraw();
    }

    if (SUCCEEDED(hr))
    {
        *bitmapWidth = static_cast<int>(bitmapSize.width);
        *bitmapHeight = static_cast<int>(bitmapSize.height);
        *surface = surfaceTile.Detach();
    }

    return hr;
}

template<typename TBase>
HRESULT D2D1Drawable<TBase>::CreateD2D1BitmapFromFile(LPCWSTR filename, ID2D1Bitmap **bitmap)
{
    HRESULT hr = (bitmap == nullptr) ? E_POINTER : S_OK;

    if (SUCCEEDED(hr))
    {
        *bitmap = nullptr;

        hr = (_wicFactory == nullptr) ? E_UNEXPECTED : S_OK;
    }

    CComPtr<IWICBitmapDecoder> wicBitmapDecoder;

    if (SUCCEEDED(hr))
    {
        hr = _wicFactory->CreateDecoderFromFilename(
            filename,
            nullptr,
            GENERIC_READ,
            WICDecodeMetadataCacheOnLoad,
            &wicBitmapDecoder);
    }

    CComPtr<IWICBitmapFrameDecode> wicBitmapFrame;

    if (SUCCEEDED(hr))
    {
        hr = wicBitmapDecoder->GetFrame(0, &wicBitmapFrame);
    }

    CComPtr<IWICFormatConverter> wicFormatConverter;

    if (SUCCEEDED(hr))
    {
        hr = _wicFactory->CreateFormatConverter(&wicFormatConverter);
    }

    if (SUCCEEDED(hr))
    {
        hr = wicFormatConverter->Initialize(
            wicBitmapFrame,
            GUID_WICPixelFormat32bppPBGRA,
            WICBitmapDitherTypeNone,
            nullptr,
            0.0f,
            WICBitmapPaletteTypeMedianCut);
    }

    CComPtr<IWICBitmap> wicBitmap;

    if (SUCCEEDED(hr))
    {
        hr = _wicFactory->CreateBitmapFromSource(wicFormatConverter, WICBitmapCacheOnLoad, &wicBitmap);
    }

    if (SUCCEEDED(hr))
    {
        hr = _d2d1DeviceContext->CreateBitmapFromWicBitmap(wicBitmap, bitmap); 
    }

    return hr;
}

template class D2D1Drawable<CWnd>;