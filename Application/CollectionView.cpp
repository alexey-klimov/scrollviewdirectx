#include "stdafx.h"
#include "CollectionView.h"
#include "CollectionItemView.h"
#include "AnimationController.h"

/*-------------------------------------------------*/

CollectionView::CollectionView()
	: m_contentOffset(0)
	, m_contentSize(0)
	, m_margin(10)
	, m_optimalElementsCount(4)
	, m_optimalElementWidth(0)
{
	collectionEventsHandler.onItemClicked = 
		[](CollectionViewItem&){};
}

/*-------------------------------------------------*/

CollectionView::~CollectionView()
{
}

/*-------------------------------------------------*/

void CollectionView::Init(CWnd & _parent, CRect& _rect)
{
	{
		static int id = 100;
		id++;
	
		this->Create(NULL, _T("collection"), WS_CHILD | WS_VISIBLE | WS_BORDER, 
			_rect, &_parent, id);
	}

	{
		m_animationController.reset(new AnimationController(m_hWnd));
		m_animationController->offsetSetter = [this](int _offset){ SetContentOffset(_offset); };
		m_animationController->offsetGetter = [this](){return GetContentOffset(); };
	}

	{
		m_optimalElementWidth = (_rect.Width() - (m_optimalElementsCount + 1) * m_margin) 
			/ m_optimalElementsCount;
		m_optimalElementHeight = _rect.Height() - 2 * m_margin;
	}

	HRESULT hr = InitD2D1Drawable();
	int bitmapWidth = 0;
	int bitmapHeight = 0;

	if (SUCCEEDED(hr))
	{
		hr = _device->CreateVisual(&m_visual);
	}

	if (SUCCEEDED(hr))
	{
		hr = _target->SetRoot(m_visual);
	}

	if (SUCCEEDED(hr))
	{
		hr = _device->Commit();
	}
}

/*-------------------------------------------------*/

void CollectionView::addCollectionItem( CollectionViewItem* _item )
{	
	CRect rect;
	this->GetClientRect(&rect);

	CSize itemSize(m_optimalElementWidth, m_optimalElementHeight);
	CPoint origin( GetContentOffset() + m_margin + 
			m_collectionItems.size() * (itemSize.cx + m_margin), 
		m_margin
	);
	HRESULT hr = _item->Init(*this, CRect(origin, itemSize), _device);
	if (SUCCEEDED(hr))
	{
		hr = m_visual->AddVisual(_item->m_visual, FALSE, nullptr);
	}
	if (SUCCEEDED(hr))
	{
		hr = _device->Commit();
	}
	m_collectionItems.push_back( 
		std::unique_ptr<CollectionViewItem>(_item) 
	);
	this->Invalidate();
}

int CollectionView::GetContentOffset() const
{
	return m_contentOffset;
}

void CollectionView::SetContentOffset(int _offset)
{
	printf("%d\n", _offset);
	m_contentOffset = _offset;
	//for (int i = 0; i < m_collectionItems.size();++i)
	//{
	//	CollectionViewItem& item = *m_collectionItems[i];
	//	CRect rect;
	//	
	//	item.GetClientRect(&rect);
	//	item.MapWindowPoints(this, &rect);

	//	rect.left = -m_contentOffset + m_margin + i * (m_optimalElementWidth + m_margin);
	//	rect.right = rect.left + m_optimalElementWidth;

	//	item.MoveWindow(&rect);
	//}

	HRESULT hr = S_OK;
	CComPtr<IDCompositionTranslateTransform> translateTransform;

	if (SUCCEEDED(hr))
	{
		hr = _device->CreateTranslateTransform(&translateTransform);
	}

	if (SUCCEEDED(hr))
	{
		hr = translateTransform->SetOffsetX((float)-m_contentOffset);
	}

	if (SUCCEEDED(hr))
	{
		hr = m_visual->SetTransform(translateTransform);
	}

	if (SUCCEEDED(hr))
	{
		hr = _device->Commit();
	}
}
	
int CollectionView::GetContentSize() const	
{
	return m_margin + m_collectionItems.size() * (m_optimalElementWidth + m_margin);
}

CRect CollectionView::GetBounds() const
{
	CRect rect;
	this->GetClientRect(rect);
	return rect;
}

/*-------------------------------------------------*/

BEGIN_MESSAGE_MAP(CollectionView, CWnd)
	ON_WM_PAINT()
	ON_WM_TIMER()
END_MESSAGE_MAP()

/*-------------------------------------------------*/

BOOL CollectionView::PreCreateWindow(CREATESTRUCT& cs) 
{
	if (!CWnd::PreCreateWindow(cs))
		return FALSE;

	cs.lpszClass = AfxRegisterWndClass(0);

	return TRUE;
}

template<class Interface>
inline void SafeRelease( Interface **ppInterfaceToRelease )
{
    if (*ppInterfaceToRelease != NULL)
    {
        (*ppInterfaceToRelease)->Release();

        (*ppInterfaceToRelease) = NULL;
    }
}

void CollectionView::OnPaint()
{
	HRESULT hr = S_OK;
	//hr = CreateDeviceResources();
	//if (!SUCCEEDED(hr))
	//	return;

	/*D2D1_SIZE_F size = m_pRenderTarget->GetSize();

	m_pRenderTarget->BeginDraw();
	m_pRenderTarget->Clear(D2D1::ColorF(D2D1::ColorF::White));
	
	for (size_t i = 0; i < m_collectionItems.size(); ++i)
	{
		CollectionViewItem& item = *m_collectionItems[i];
		CRect rect;
		
		item.GetClientRect(&rect);
		item.MapWindowPoints(this, &rect);
		
		D2D1_RECT_F dxRect = D2D1::RectF(
			rect.left - this->GetContentOffset(),
			rect.top,
			rect.right - this->GetContentOffset(),
			rect.bottom
		);

		m_pRenderTarget->FillRectangle(&dxRect, m_pLightSlateGrayBrush);
	}
    hr = m_pRenderTarget->EndDraw();*/

  //  if (hr == D2DERR_RECREATE_TARGET)
  //  {
  //      hr = S_OK;
		//DiscardDeviceResources();
  //  }

	this->ValidateRect(nullptr);
}

void CollectionView::OnTimer(UINT)
{
	m_animationController->onTimer();
}

/*-------------------------------------------------*/

void CollectionView::StartScrolling(bool _forward)
{
	if (m_collectionItems.empty())
		return;

	m_animationController->stop();
	m_animationController->start(_forward);
}

void CollectionView::StopScrolling()
{
	bool previousScrollingWasForward = m_animationController->isForward();
	if (m_collectionItems.empty())
		return;
	
	m_animationController->stop();
	
	int toValueX = GetContentOffset();
	if ((GetContentOffset() < 0))
		toValueX = 0;
	else if (GetBounds().Width() > GetContentSize())
		toValueX = 0;
	else if (GetContentSize() < (GetContentOffset() + GetBounds().Width()) )
		toValueX = GetContentSize() - GetBounds().Width();
	else
		toValueX = GetContentOffset() - GetContentOffset() % (m_optimalElementWidth + m_margin)
			+ (!previousScrollingWasForward ? 0 : m_optimalElementWidth + m_margin);
	if (toValueX != GetContentOffset())
		m_animationController->startPrecisely(toValueX);
}

/*-------------------------------------------------*/